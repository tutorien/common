# Common Latex Commands
## 2019 Lucas Alber

Requires `lualatex` and the `beamer` document class.

# Configuration

Edit the `tut-config` file.

# Usage
Include the wanted feature sets:
```
% Common commands and layout configuration
\usepackage{common/tut-common}
% Commands for creating graphs (tikz)
\usepackage{common/algo}
```

# License

The project itself is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. This means that if you change the sources and re-distribute them, you must retain the copyright notice header and license it under the same CC-BY-SA license. This does not affect the presentation that you create with the project.

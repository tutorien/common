% ---------------------------------------------------------------------------
% Copyright 2019 Lucas Alber
%
% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License (https://creativecommons.org/licenses/by-sa/4.0/).
% ---------------------------------------------------------------------------
\ProvidesPackage{algo}
\RequirePackage{calc}
\RequirePackage{amsmath}
\RequirePackage{mathtools}
\RequirePackage{tikz}
\RequirePackage{etoolbox}
\RequirePackage{fp}
\RequirePackage[ruled,vlined,lined,boxed,linesnumbered,commentsnumbered]{algorithm2e}
\usetikzlibrary{tikzmark, calc, matrix, graphs,
                positioning, patterns, arrows.meta,
                chains, trees, decorations.pathreplacing}
				
% 'to'-keyword for for-loops
\newcommand\To{
    \textbf{to} % intentional space
}

%Gauss Brackets
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% absolute and norm
\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


% Tree formatting
\tikzset{
    level distance=1.5cm,
    level 1/.style={sibling distance=3cm},
    level 2/.style={sibling distance=1.5cm},
    level 3/.style={sibling distance=0.75cm}}
	
% Matrix formatting
\tikzset{
    table/.style = {
        matrix of nodes,
        row sep=-\pgflinewidth,
        column sep=-\pgflinewidth,
        nodes={rectangle,draw=black,text width=4.00ex,align=center},
        text depth=0.25ex,
        text height=1.7ex,
        nodes in empty cells,
        ampersand replacement=\&
    },
	widetable/.style = {
		matrix of nodes,
		row sep=-\pgflinewidth,
		column sep=-\pgflinewidth,
		nodes={rectangle,draw=black,text width=8.00ex,align=center},
		text depth=0.25ex,
		text height=1.7ex,
		nodes in empty cells,
		ampersand replacement=\&
	},
    mathmatrix/.style= {
        matrix of math nodes,
        left delimiter={[},
        right delimiter={]},
        ampersand replacement=\&
    }
}

% Edge styles
\tikzset{T/.style args = {#1}{draw,
        to path={|- node[pos=#1] {}
            (\tikztotarget)}},
    T/.default = 1}
\tikzset{St/.style = {-{Stealth[scale=2,length=2,width=2]}}}

% Shorthand for coloring nodes labels etc. differently across slides
\newcommand\clo[1]{%
    \foreach \clr/\pagenum/\content in {#1}{%
        % treat color 'none' differently
        \expandafter\ifstrequal\expandafter{\clr}{none}{%
            \only<\pagenum>{\content}%
        }{%
            \only<\pagenum>{\textcolor{\clr}{\content}}%
        }%
    }%
}

% Graph node styles
\tikzset{stdnode/.style={shape=circle, draw=black},
    visnode/.style={shape=circle, draw=black, fill=green!70},
    stduedge/.style={line width=1pt},
    curuedge/.style={line width=1pt, green},
    visuedge/.style={line width=1pt, cyan},
    stdedge/.style={line width=1pt, St},
    curedge/.style={line width=1pt, St, green},
    visedge/.style={line width=1pt, St, cyan}
}

% edge macro (directed)
% - Params: src, dest, active-on (may be -)
\newcommand{\drawedge}[3]{%
    \drawedgeinternal{#1}{#2}{#3}{-}{-}{}{stdedge}{curedge}{visedge}%
}

% edge macro (directed, bend)
% - Params: src, dest, active-on (may be -), bend, bend amount
\newcommand{\drawedgebend}[5]{%
    \drawedgeinternal{#1}{#2}{#3}{#4}{#5}{}{stdedge}{curedge}{visedge}%
}

% edge macro (directed, bend, labeled)
% - Params: src, dest, active-on (may be -), bend, bend amount, label
\newcommand{\drawedgebendlabel}[6]{%
	\drawedgeinternallabel{#1}{#2}{#3}{#4}{#5}{#6}{stdedge}{curedge}{visedge}%
}

% edge macro (directed, labeled)
% - Params: src, dest, active-on (may be -), label
\newcommand{\drawedgelabel}[4]{%
	\drawedgeinternallabel{#1}{#2}{#3}{-}{-}{#4}{stdedge}{curedge}{visedge}%
}

% edge macro (directed, labeled)
% - Params: src, dest, active-on (may be -), label
\newcommand{\drawuedgelabel}[4]{%
	\drawedgeinternallabel{#1}{#2}{#3}{-}{-}{#4}{stduedge}{curuedge}{visuedge}%
}

% edge macro (directed, labeled)
% - Params: src, dest, active-on (may be -), label
\newcommand{\drawuedgelabelinactive}[4]{%
	\drawedgeinternallabel{#1}{#2}{#3}{-}{-}{#4}{stduedge}{curuedge}{stdedge}%
}

% edge macro (undirected)
% - Params: src, dest, active-on (may be -)
\newcommand{\drawuedge}[3]{%
    \drawedgeinternal{#1}{#2}{#3}{-}{-}{}{stduedge}{curuedge}{visuedge}%
}

% edge macro (internal)
% - Params: src, dest, active-on, bend, bend-amount, label, edgestyles1-3
\newcommand{\drawedgeinternal}[9]{%
    \expandafter\ifstrequal\expandafter{#3}{-}{%
        \expandafter\ifstrequal\expandafter{#4}{-}{%
            \draw(#1) edge[#7] (#2);
        }{%
            \draw(#1) edge[#7, bend #4=#5] (#2);
        }
    }{%
        \expandafter\ifstrequal\expandafter{#4}{-}{%
            \FPeval{\preact}{clip(#3-1)}
            \FPeval{\postact}{clip(#3+1)}
            \draw<-\preact> (#1) edge[#7] (#2);
            \draw<#3> (#1) edge[#8] (#2);
            \draw<\postact-> (#1) edge[#9] (#2);
        }{%
            \FPeval{\preact}{clip(#3-1)}
            \FPeval{\postact}{clip(#3+1)}
            \draw<-\preact> (#1) edge[#7, bend #4=#5] (#2);
            \draw<#3> (#1) edge[#8, bend #4=#5  (#2);
            \draw<\postact-> (#1) edge[#9, bend #4=#5] (#2);
        }
    }%
}

% edge macro (internal)
% - Params: src, dest, active-on, bend, bend-amount, label, edgestyles1-3
\newcommand{\drawedgeinternallabel}[9]{%
	\expandafter\ifstrequal\expandafter{#3}{-}{%
		\expandafter\ifstrequal\expandafter{#4}{-}{%
			\draw(#1) edge[#7] node[circle, midway, fill=white]{#6} (#2);
		}{%
			\draw(#1) edge[#7, bend #4=#5] node[circle, midway, fill=white]{\small{#6}} (#2);
		}
	}{%
		\expandafter\ifstrequal\expandafter{#4}{-}{%
			\FPeval{\preact}{clip(#3-1)}
			\FPeval{\postact}{clip(#3+1)}
			\draw<-\preact> (#1) edge[#7] node[circle, midway, fill=white]{\small{#6}} (#2);
			\draw<#3> (#1) edge[#8] node[circle, midway, fill=white]{\small{#6}} (#2);
			\draw<\postact-> (#1) edge[#9] node[circle, midway, fill=white]{\small{#6}} (#2);
		}{%
			\FPeval{\preact}{clip(#3-1)}
			\FPeval{\postact}{clip(#3+1)}
			\draw<-\preact> (#1) edge[#7, bend #4=#5] node[circle, midway, fill=white]{\small{#6}} 
			(#2);
			\draw<#3> (#1) edge[#8, bend #4=#5] node[circle, midway, fill=white]{\small{#6}} (#2);
			\draw<\postact-> (#1) edge[#9, bend #4=#5] node[circle, midway, fill=white]{\small{#6}} 
			(#2);
		}
	}%
}

% node macro
% - Params: name, x, y, visited-on (may be -)
\newcommand{\drawnode}[4]{%
    \expandafter\ifstrequal\expandafter{#4}{-}{%
        \node[stdnode] (#1) at (#2,-#3) {#1};
    }{%
        \FPeval{\unvis}{clip(#4-1)}
        \node<-\unvis>[stdnode] (#1) at (#2,-#3) {#1};
        \node<#4->[visnode] (#1) at (#2,-#3) {#1};
    }
}

% advanced node macro
% - Params: name, x, y, visited-on (may be -), label
\newcommand{\drawnodeadv}[5]{%
	\expandafter\ifstrequal\expandafter{#4}{-}{%
		\node[stdnode] (#1) at (#2,-#3) {#5};
	}{%
		\FPeval{\unvis}{clip(#4-1)}
		\node<-\unvis>[stdnode] (#1) at (#2,-#3) {#5};
		\node<#4->[visnode] (#1) at (#2,-#3) {#5};
	}
}

% 'Stop jumping'-style
% See
% https://tex.stackexchange.com/questions/18704/how-can-i-fix-jumping-tikz-pictures-in-beamer
\newcounter{jumping}
\resetcounteronoverlays{jumping}
\makeatletter
\tikzset{
  stop jumping/.style={
    execute at end picture={%
      \stepcounter{jumping}%
      \immediate\write\pgfutil@auxout{%
        \noexpand\jump@setbb{\the\value{jumping}}{\noexpand\pgfpoint{\the\pgf@picminx}{\the\pgf@picminy}}{\noexpand\pgfpoint{\the\pgf@picmaxx}{\the\pgf@picmaxy}}
      },
      \csname jump@\the\value{jumping}@maxbb\endcsname
      \path (\the\pgf@x,\the\pgf@y);
      \csname jump@\the\value{jumping}@minbb\endcsname
      \path (\the\pgf@x,\the\pgf@y);
    },
  }
}
\def\jump@setbb#1#2#3{%
  \@ifundefined{jump@#1@maxbb}{%
    \expandafter\gdef\csname jump@#1@maxbb\endcsname{#3}%
  }{%
    \csname jump@#1@maxbb\endcsname
    \pgf@xa=\pgf@x
    \pgf@ya=\pgf@y
    #3
    \pgfmathsetlength\pgf@x{max(\pgf@x,\pgf@xa)}%
    \pgfmathsetlength\pgf@y{max(\pgf@y,\pgf@ya)}%
    \expandafter\xdef\csname jump@#1@maxbb\endcsname{\noexpand\pgfpoint{\the\pgf@x}{\the\pgf@y}}%
  }
  \@ifundefined{jump@#1@minbb}{%
    \expandafter\gdef\csname jump@#1@minbb\endcsname{#2}%
  }{%
    \csname jump@#1@minbb\endcsname
    \pgf@xa=\pgf@x
    \pgf@ya=\pgf@y
    #2
    \pgfmathsetlength\pgf@x{min(\pgf@x,\pgf@xa)}%
    \pgfmathsetlength\pgf@y{min(\pgf@y,\pgf@ya)}%
    \expandafter\xdef\csname jump@#1@minbb\endcsname{\noexpand\pgfpoint{\the\pgf@x}{\the\pgf@y}}%
  }
}
\makeatother